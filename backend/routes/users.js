const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');

const UserModel = require('../models/User');

router.get('/', async (req, res) => {
	const users = await UserModel.find();
		res.send(users);
	});

router.get('/:id', async (req, res) => {
	const users = await UserModel.findById(req.params.id);
		res.send(users);
	});


router.post('/', async (req, res) => { 

		const salt = await bcrypt.genSalt(10);
		const hashedPassword = await bcrypt.hash(req.body.password, salt);



		let user = new UserModel ({ 
			email: req.body.email,
			password: req.body.password,
			name: {
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				middleName: req.body.middleName
			}  
		});

		user = await user.save();

		res.send(user);
	});

router.put('/:id', async (req, res) => {
	let user = await UserModel.findByIdAndUpdate(req.params.id,{
			email: req.body.email,
			password: req.body.password,
			name: {
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				middleName: req.body.middleName
			}
	}, {new:true});

	res.send(user);
});

router.delete('/:id', async (req, res) => {
	let user = await UserModel.findByIdAndRemove(req.params.id);
	res.send(user);
});

// router.exports('/', async (req, res) => {
// 	let users = await UserModel.find()
// 		console.log(users);
// 		res.send(users);
// 	});


module.exports = router;

