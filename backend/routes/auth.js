const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const express = require('express');
const router = express.Router();

const UserModel = require('../models/User');
const config = require('../config.js');

router.post('/', async (req, res) => {
		
		let user = await UserModel.findOne ({ email:res.body.email });
		
		if(!user) {
			return res.status(400).send({
				message: "Username of Password is invalid"
			});

		}

		let isMatch = await	bcrypt.compare(req.body.password, user.password);

		
		if(!isMatch) {
			return res.status(400).send({
				message: "Username of Password is invalid"
			});

		}

		const payload = {
			_id: user._id,
			email: user.email,
			fullname: `${user.name.first} ${user.name.last}`
		};

		const token = jwt.sign(payload, config.secret);

		res.header('x-auth-token', token).send(user);
		
	});


module.exports = router;