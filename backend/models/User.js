const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	email: String,
	password: String,
	name: {
		firstName: String,
		lastName: String,
		middleName: String
	}
	 
});

const User = mongoose.model('User', UserSchema);

module.exports = User;




