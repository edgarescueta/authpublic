const mongoose = require('mongoose');	//requiring the mongoose
const Schema = mongoose.Schema;			

const TaskSchema = new Schema({
    	taskName: String,
    	created_at: { type: Date, default: Date.now() },
    	isDone: { type: Boolean, default:false},
    	subTasks: [
    		{
    			taskName: String,
    			created_at: { type: Date, default: Date.now() },
    			isDone: { type:Boolean, default: false}

    		}
    	]
    
});


const TaskModel = mongoose.model('Task', TaskSchema);			

module.exports = TaskModel;