module.exports = {
	port : process.env.PORT || 3000,
	connectionString: process.env.DATABASE_URL || 'mongodb+srv://admin_01:admin_01@cluster0-klkoz.mongodb.net/taskr?retryWrites=true&w=majority',
	secret: process.env.SECRET_KEY || "taskr@S3CR3T"
}

