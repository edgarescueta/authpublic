const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();

const tasks = require('./routes/tasks');
const users = require('./routes/users');
const auth = require('./routes/auth');

const config = require('./config');

app.use(cors());

mongoose.connect(config.connectionString, {useNewUrlParser: true }, () => {
	console.log('remote connection established!')
});

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use('/api/tasks', tasks);
app.use('/api/users', users);
app.use('/api/auth', auth);

app.listen(config.port, () => {
	console.log(`The server is listening to port: ${config.port}`)
});