<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function index()
    {
    	return view('auth.login');
    }
    public function login(Request $request) 
    {
    	$client	= new Client;
    	$response = $client->post('localhost:3000/api/auth', [
    		"json" => [
    			"email" => $request->email,
    			"password" => $request->password
    		]
		]);
		
		Session::put('token', $response->getHeader('x-auth-token')[0]);
		Session::put('user', json_decode($response->getBody()));

		return redirect('/');

    }
}
