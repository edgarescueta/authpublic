@extends('layouts.app')

@section('content')

<div class="container">
	<div class="card mx-auto my-2" style="width: 25rem;">
		<div class="card-header bg-dark text-light">
			Tasks
		</div>
		
		<div class="card-body">
			<form action="/tasks" method="POST">
				@csrf
				<input type="text" class="form-control" name="newtask">
				<button class="btn btn-primary btn-block my-1">Add</button>

			</div>
		</form>

		<ul class="list-group list-group-flush">
			@forelse($tasks as $task)
			<li class="list-group-item">{{$task->taskName}}

				<form action="/tasks/{{$task->_id}}" method="POST">
					@csrf
					@method('PUT')
					<input type="text" name="task" class="form-control">
					<button class="btn btn-primary">Update</button>
				</form>


				<form action="/tasks/{{$task->_id}}" method="POST">
					@csrf
					@method('DELETE')
					<button class="btn btn-danger">Delete</button>
				</form>
			</li>

			@forelse($task->subTasks as $subTask)
			<li class="list-group-item"> -> {{$subTask->taskName}}
				<button class="btn btn-primary">Edit</button>
			</li>
			@empty

			@endforelse

			@empty
			No Item
			@endforelse
		</ul>
	</div>

</div>

@endsection